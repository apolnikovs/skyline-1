# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.270');

# ---------------------------------------------------------------------- #
# Modify Table service_provider_engineer_details                         #
# ---------------------------------------------------------------------- # 
ALTER TABLE `service_provider_engineer_details`
	ADD COLUMN `SliderStartShift` TIME NULL AFTER `Status`,
	ADD COLUMN `SliderEndShift` TIME NULL AFTER `SliderStartShift`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.271');
