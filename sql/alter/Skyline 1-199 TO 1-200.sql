# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.199');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer` ADD COLUMN `ViamenteStartType` ENUM('Fixed','Flexible') NOT NULL DEFAULT 'Flexible' AFTER `ExcludeFromAppTable`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` DROP COLUMN `ViamenteStartTime`;

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                        #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` ADD COLUMN `ConfirmedByUser` ENUM('Confirmed','Not Confirmed') NULL DEFAULT 'Not Confirmed' AFTER `SortOrder`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.200');
