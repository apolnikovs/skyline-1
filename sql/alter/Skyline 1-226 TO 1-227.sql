# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.226');


# ---------------------------------------------------------------------- #
# Modify Table model                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE model ADD COLUMN Features VARCHAR(50) NULL AFTER ModifiedDate, 
				  ADD COLUMN IMEILengthFrom INT NULL AFTER Features, 
				  ADD COLUMN IMEILengthTo INT NULL AFTER IMEILengthFrom, 
				  ADD COLUMN MSNLengthFrom INT NULL AFTER IMEILengthTo, 
				  ADD COLUMN MSNLengthTo INT NULL AFTER MSNLengthFrom, 
				  ADD COLUMN WarrantyRepairLimit DECIMAL(10,4) NULL AFTER MSNLengthTo, 
				  ADD COLUMN ExcludeFromRRCRepair ENUM('Yes','No') NULL DEFAULT 'No' AFTER WarrantyRepairLimit, 
				  ADD COLUMN AllowIMEIAlphaChar ENUM('Yes','No') NULL DEFAULT 'No' AFTER ExcludeFromRRCRepair, 
				  ADD COLUMN UseReplenishmentProcess ENUM('Yes','No') NULL DEFAULT 'No' AFTER AllowIMEIAlphaChar, 
				  ADD COLUMN HandsetWarranty1Year ENUM('Yes','No') NULL DEFAULT 'No' AFTER UseReplenishmentProcess, 
				  ADD COLUMN ReplacmentValue DECIMAL(10,4) NULL AFTER HandsetWarranty1Year, 
				  ADD COLUMN ExchangeSellingPrice DECIMAL(10,4) NULL AFTER ReplacmentValue, 
				  ADD COLUMN LoanSellingPrice DECIMAL(10,4) NULL AFTER ExchangeSellingPrice, 
				  ADD COLUMN RRCOrderCap INT NULL AFTER LoanSellingPrice;



# ---------------------------------------------------------------------- #
# Modify Table accessory                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE accessory ADD COLUMN UnitTypeID INT NOT NULL AFTER ModifiedDate;

# ---------------------------------------------------------------------- #
# Modify Table accessory_model                                           #
# ---------------------------------------------------------------------- #
CREATE TABLE accessory_model (
								AccessoryModelID INT(10) NOT NULL AUTO_INCREMENT, 
								ModelID INT(10) NOT NULL DEFAULT '0', 
								AccessoryID INT(10) NOT NULL DEFAULT '0', 
								PRIMARY KEY (AccessoryModelID) 
							  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Modify Table colour                                                    #
# ---------------------------------------------------------------------- #
CREATE TABLE colour (
						ColourID INT(10) NOT NULL AUTO_INCREMENT, 
						ColourName VARCHAR(50) NULL DEFAULT NULL, 
						Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', 
						PRIMARY KEY (ColourID)
					) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Modify Table colour_model                                              #
# ---------------------------------------------------------------------- #
CREATE TABLE colour_model (
							ColourModelID INT(10) NOT NULL AUTO_INCREMENT, 
							ColourID INT(10) NOT NULL DEFAULT '0', 
							ModelID INT(10) NOT NULL DEFAULT '0', 
							PRIMARY KEY (ColourModelID)
						  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Modify Table product_code                                              #
# ---------------------------------------------------------------------- #
CREATE TABLE product_code (
							ProductCodeID INT(10) NOT NULL AUTO_INCREMENT, 
							ProductCodeName VARCHAR(50) NOT NULL DEFAULT '0', 
							Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', 
							PRIMARY KEY (ProductCodeID)
						  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Modify Table product_code_model                                        #
# ---------------------------------------------------------------------- #
CREATE TABLE product_code_model (
									ProductCodeModelID INT(10) NOT NULL AUTO_INCREMENT, 
									ModelID INT(10) NULL, 
									ProductCodeID INT(10) NULL, 
									PRIMARY KEY (ProductCodeModelID)
								) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Add Function statustat                                                 #
# ---------------------------------------------------------------------- #

DROP FUNCTION IF EXISTS statustat;

DELIMITER $$

CREATE FUNCTION statustat(jobID INT)

RETURNS INT

DETERMINISTIC

BEGIN

	DECLARE result INT;
	DECLARE serviceProviderID INT;
	DECLARE statusDate DATE;
	DECLARE statusTime TIME;
	DECLARE diff TIME;
	
	SET serviceProviderID = (SELECT job.ServiceProviderID FROM job WHERE job.JobID = jobID);
	SET statusDate = 	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%Y-%m-%d") AS DATE)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);
	SET statusTime =	(
							SELECT 		CAST(DATE_FORMAT(status_history.Date, "%H:%i:%s") AS TIME)
							FROM		job 
							LEFT JOIN	status_history ON job.JobID = status_history.JobID
							WHERE 		job.JobID = jobID AND status_history.StatusID = job.StatusID
							LIMIT		1
						);

	SET @normalOpen := CAST((SELECT sp.WeekdaysOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	SET @normalClose := CAST((SELECT sp.WeekdaysCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#normal working day in seconds
	SET @normalDay := TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));

	#SET @satOpen := CAST((SELECT sp.SaturdayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @satClose := CAST((SELECT sp.SaturdayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);

	#SET @sunOpen := CAST((SELECT sp.SundayOpenTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	#SET @sunClose := CAST((SELECT sp.SundayCloseTime FROM service_provider AS sp WHERE sp.ServiceProviderID = serviceProviderID LIMIT 1) AS TIME);
	
	
	SET @tatType :=	(
						SELECT	status.StatusTATType 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	SET @tatTemp :=	(
						SELECT	status.StatusTAT 
						FROM 	status 
						WHERE 	status.StatusID = (SELECT job.StatusID FROM job WHERE job.JobID = jobID)
						LIMIT	1
					);
	
	IF (@tatType = "Minutes") THEN SET @tatTime := @tatTemp * 60;
	ELSEIF (@tatType = "Hours") THEN SET @tatTime := @tatTemp * 3600;
	ELSEIF (@tatType = "Days") THEN SET @tatTime := @tatTemp * @normalDay;
	END IF;
	
	
	IF DATEDIFF(CURDATE(), statusDate) = 0
	
	THEN 
	
		SET result := 0;
		
	ELSE
	
		BEGIN

			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = statusDate) > 0)
			THEN 
				#bank holiday
				SET @firstDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@sunClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, statusTime))); END IF;
					IF (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(statusDate, "%w") = 6)
			THEN
				BEGIN
					#saturday 
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@satClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @firstDay := TIME_TO_SEC(TIMEDIFF(@normalClose, statusTime));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, statusTime))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(statusTime, @normalClose))); END IF;
				END;
			END IF;			
			
			IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = CURDATE()) > 0)
			THEN 
				#bank holiday
				SET @lastDay := 0;
			/*	
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 0)
			THEN
				BEGIN
					#sunday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@sunOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @sunClose))); END IF;
				END;
			ELSEIF (DATE_FORMAT(CURDATE(), "%w") = 6)
			THEN 
				BEGIN
					#saturday
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @satOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@satOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @satClose))); END IF;
				END;
			*/
			ELSE 
				BEGIN
					#working day
					SET @lastDay := TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalOpen));
					#account for time difference if status was changed outside working hours
					IF(TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME())) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(@normalOpen, CURTIME()))); END IF;
					IF(TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose)) > 0) THEN SET @firstDay := @firstDay - (TIME_TO_SEC(TIMEDIFF(CURTIME(), @normalClose))); END IF;
				END;
			END IF;			
						
			
			SET @diffSeconds := @firstDay + @lastDay;

						
			SET @daysDiff := DATEDIFF(CURDATE(), statusDate);
			
			IF @daysDiff > 1
			THEN
				BEGIN
					
					SET @curDay := 1;
					
					SET @seconds := 0;
					
					WHILE @curDay < @daysDiff DO
						
						SET @curDayDate := INTERVAL @curDay DAY + statusDate;
						
						IF ((SELECT COUNT(*) FROM national_bank_holiday AS nh WHERE nh.HolidayDate = @curDayDate) > 0)
						THEN 
							#bank holiday
							SET @diffSeconds := @diffSeconds;
						/*	
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 0)
						THEN
							#sunday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@sunClose, @sunOpen));
						ELSEIF (DATE_FORMAT(@curDayDate, "%w") = 6)
						THEN 
							#saturday
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@satClose, @satOpen));
						*/
						ELSE 
							#working day
							SET @diffSeconds := @diffSeconds + TIME_TO_SEC(TIMEDIFF(@normalClose, @normalOpen));
						END IF;			
						
						SET @curDay := @curDay + 1;
						
					END WHILE;
					
				END;
			END IF;
			
			
			IF (@diffSeconds > @tatTime) 
			THEN SET result = @daysDiff;
			ELSE SET result = -1;
			END IF;
			
		END;
		
	END IF;

	
	RETURN result;

END$$



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.227');
