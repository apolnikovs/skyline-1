# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.289');

# ---------------------------------------------------------------------- #
# Andris Stock Changes     												 #
# ---------------------------------------------------------------------- # 
CREATE TABLE sp_stock_receiving_history ( SpStockReceivingHistoryID INT(10) NOT NULL AUTO_INCREMENT, MovementType ENUM('Incoming','Outgoing') NOT NULL DEFAULT 'Incoming', DateReceived TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, SPPartOrderID INT(11) NULL DEFAULT NULL, JobID INT(11) NULL DEFAULT NULL, UserID INT(11) NULL DEFAULT NULL, ServiceProviderSupplierID INT(11) NULL DEFAULT NULL, QtyReceived INT(11) NULL DEFAULT NULL, DespatchNo INT(11) NULL DEFAULT NULL, InvoiceNo VARCHAR(30) NULL DEFAULT NULL, UnitPrice DECIMAL(10,2) NOT NULL DEFAULT '0.00', ExchangeRate DECIMAL(10,2) NOT NULL DEFAULT '0.00', Notes VARCHAR(250) NOT NULL, QtyOrdered INT(10) NULL DEFAULT NULL, PRIMARY KEY (SpStockReceivingHistoryID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE appointment ADD COLUMN ViamenteUnreachedTimestamp TIMESTAMP NULL AFTER ViamenteUnreached;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.290');
