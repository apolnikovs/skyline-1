<REPAIR_STATUS>
	<REPAIR_STATUS_INFO>
		<COMPANY>{$nsp.CompanyCode}</COMPANY>
		<TR_NO>{$job.NetworkRefNo}</TR_NO>
		<ASC_CODE>{$nsp.AccountNo}</ASC_CODE>
		<ASC_JOB_NO>{$job.ServiceCentreJobNo}</ASC_JOB_NO>
		<MODEL_CODE>{$model_code}</MODEL_CODE>
		<SERIAL_NO>{$job.SerialNo}</SERIAL_NO>
		<IMEI_NO></IMEI_NO>
                {foreach $tr_status as $status}
		<TR_STATUS>{$status}</TR_STATUS>
                {/foreach}
                {foreach $tr_reason as $reason}
		<TR_REASON>{$reason}</TR_REASON>
                {/foreach}
          <STATUS_COMMENT>{$job.AgentStatus}</STATUS_COMMENT>
		<WARRANTY_LABOR>{$job.SamsungWarrantyLabour}</WARRANTY_LABOR>
		<WARRANTY_PARTS>{$job.SamsungWarrantyParts}</WARRANTY_PARTS>
		<PRODUCT_RECEIVED_DATE>{$job.DateUnitReceived|date_format:'%Y%m%d'}</PRODUCT_RECEIVED_DATE>
		<PRODUCT_RECEIVED_TIME>{$smarty.now|date_format:'%H%M%S'}</PRODUCT_RECEIVED_TIME>
		<REPAIR_ETD_DATE>{$job.ETDDate|date_format:'%Y%m%d'}</REPAIR_ETD_DATE>
		<REPAIR_ETD_TIME>090000</REPAIR_ETD_TIME>
		<ENGINEER_CODE>{$job.EngineerCode}</ENGINEER_CODE>
		<ENGINEER_NAME>{$job.EngineerName}</ENGINEER_NAME>
		<ENG_ASSIGN_DATE>{$eng_assign_date|date_format:'%Y%m%d'}</ENG_ASSIGN_DATE>
		<ENG_ASSIGN_TIME>{$eng_assign_time|date_format:'%H%M%S'}</ENG_ASSIGN_TIME>
		<VISIT_DATE>{$appointment.AppointmentDate|date_format:'%Y%m%d'}</VISIT_DATE>
		<VISIT_TIME>{$appointment.AppointmentTime|date_format:'%H%M%S'}</VISIT_TIME>
		<REPAIR_COMPLETE_DATE>{$job.RepairCompleteDate}</REPAIR_COMPLETE_DATE>
		<REPAIR_COMPLETE_TIME>{if isset($job.RepairCompleteDate)}090000{/if}</REPAIR_COMPLETE_TIME>
		<GOODS_DELIVERY_DATE>{$job.ClosedDate|date_format:'%Y%m%d'}</GOODS_DELIVERY_DATE>
		<GOODS_DELIVERY_TIME>{if isset($job.RepairCompleteDate)}090000{/if}</GOODS_DELIVERY_TIME>
		<OUTBOUND_SHIPPING_NO></OUTBOUND_SHIPPING_NO>
		<SHOP_RETURN_DATE></SHOP_RETURN_DATE>
		<SHOP_RETURN_TIME></SHOP_RETURN_TIME>
		<CONSUMER_RECEIVED_DATE></CONSUMER_RECEIVED_DATE>
		<CONSUMER_RECEIVED_TIME></CONSUMER_RECEIVED_TIME>
		<QNA_NO></QNA_NO>
		<GRMS_NO></GRMS_NO>
          <WTY_EXCEPTION></WTY_EXCEPTION>
	</REPAIR_STATUS_INFO>
	<REPAIR_PART_INFO>
            {foreach $parts as $part}
		<TR_NO>{$job.NetworkRefNo}</TR_NO>
		<PARTS_ORDER_NO>{$part.SupplierOrderNo}</PARTS_ORDER_NO>
		<PART_NO>{$part.PartNo}</PART_NO>
		<PART_QTY>{$part.Quantity}</PART_QTY>
                <PART_STATUS>{$part.status}</PART_STATUS>
            {foreachelse}
                    <TR_NO />
                    <PARTS_ORDER_NO />
                    <PART_NO />
                    <PART_QTY />
                    <SO_ITEM />
                    <PART_STATUS />
            {/foreach}
        </REPAIR_PART_INFO>
</REPAIR_STATUS>