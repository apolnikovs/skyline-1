 <script type="text/javascript">   
     
 $(document).ready(function() {

 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });   
    
   }); 
   
  </script>  
    
    <div id="PartSubCategoryFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartSubCategoryForm" name="PartSubCategoryForm" method="post"  action="{$_subdomain}/LookupTables/savePartSubCategory" class="inline" >
                    <input type="hidden" name="ServiceProviderPartSubCategoryID" value="{$datarow.ServiceProviderPartSubCategoryID|default:""}">
                <fieldset>
                    <legend title="" >Part Category</legend>
                        
                 
                            
                        
                        
                        
 
          <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
          <div id="tabs-1" class="SystemAdminFormPanel inline">
              
          <p>
                                <label class="cardLabel" for="SubCategoryName" >Sub-Category Name:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="SubCategoryName" value="{$datarow.SubCategoryName|default:''}" id="SubCategoryName" >
          </p>
           <p>
                                <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
                                    
				<input  type="checkbox" name="Status"  value="In-active" {if $datarow.Status|default:'' eq 'In-active'}checked="checked"{/if}  /><label >Inactive</label>&nbsp;
					</span>

                                    
          </p>
          <p>
                            <label class="cardLabel" for="ServiceProviderPartCategoryID" >Category:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                                <select name="ServiceProviderPartCategoryID" id="RepairTypeID" class="text" >
                                <option value="" {if $datarow.ServiceProviderPartCategoryID|default:"" eq ''}selected="selected"{/if}>Select from drop down</option>

                                
                                {foreach $categories|default:"" as $s}

                                    <option value="{$s.ServiceProviderPartCategoryID|default:""}" {if $datarow.ServiceProviderPartCategoryID|default:"" eq $s.ServiceProviderPartCategoryID|default:""}selected="selected"{/if}>{$s.CategoryName|default:""}</option>

                                {/foreach}
                                
                            </select>
      </p>
          
          
         
          
          
          
          
          </div>
          
   
      
          
  
  <hr>
                               
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>

                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
