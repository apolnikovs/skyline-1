 <div id="popUpContainer" >
             <div id="appDetails" >
         <fieldset>
             <legend>Add new diary allocation slot</legend>
              <form method=post action="{$_subdomain}/Diary/addDiarySlot">
                  <input type="hidden"  name="dat" value="{$day}">
                  
                  <div style="text-align: center;color:#6b6b6b;font-size:18px;">Date: {$day|date_format:"%e/%m/%Y"}</div>
             <table>
                 <tr>
                     <td>Time Slot</td><td>
                     
                     <td>
                         {foreach $times as $tt} 
                          {if $tt.Type!="ANY"}<input    type="radio" value="{$tt.AppointmentAllocationSlotID}" name="timeSlot"> {$tt.Description}{/if}
                         {/foreach}
                 
                   
                     </td>
                 </tr>
                 <tr>
                     <td>Number of Allocations</td><td>
                     
                     <td>
                  <input  type="Text"  name="NumberOfAllocations" size="3"> 
                  
                   
                     </td>
                 </tr>
                 <tr>
                     <td>Postcodes, Separated by ","</td><td>
                     
                     <td>
                  <textarea   name="Postcodes"></textarea>
                         
                     </td>
                 </tr>
                 <tr>
                     <td>Allocated Engineer</td><td>
                     
                     <td>
                         <select name="ServiceProviderEngineerID">
                             <option></option>
                             {foreach $engList as $ff}
                              <option  value="{$ff.ServiceProviderEngineerID}">{$ff.EngineerFirstName} {$ff.EngineerLastName}</option>
                             {/foreach}
                         </select>
                         
                     </td>
                 </tr>
                 
                 </table
 </form>
<button type="submit" class="gplus-blue" name="add" value="add"  style="float:left">Add new allocation</button>
<button type="button" class="gplus-blue" onclick="$.colorbox.close()"  style="background:red;color:white;float:right">Cancel</button>
    </fieldset>
   
    </div>
 
<!--        end job  details field set    -->
     
 
         