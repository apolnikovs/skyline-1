<script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>

<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />


<script type="text/javascript">  
    $(document).ready(function(){ 
        $('#unsentemail').change(function() {
            $('#sentemail').find("option").attr("selected", false);
            $('#EmailID').val($('#unsentemail').val());
        });
        
        $('#sentemail').change(function() {
            $('#unsentemail').find("option").attr("selected", false);
            $('#EmailID').val($('#sentemail').val());
        });
        
        $('#cancel_email_btn').click(function() {
        
            $.colorbox.close();
            
        });
        
        $('#send_email_btn').click(function() {
        
            return false;
            
        });
        
        
        $('#resend_email_btn').click(function() {
        
            return false;
            
        });
        
        
    });

</script>    



                                    
<div class="main" id="jobdetails">
                                    
    
    
    <p>&nbsp;</p>
    
    <div class="span24">
        <form id="EMailManager" name="EMailManager" action="#" method="post">
        <fieldset>
            <legend>EMail Manager</legend>
            
            To send email message to your customer select an option below.
            
            <p>
            
            <span class="span-9 first">
                Pre-defined Email Messages
                
                <select size="11" id="unsentemail" name="unsentemail">
                    
                </select>
            </span>
            <span class="span-9 last">
                Previously Sent Email Messages
                <select size="11" id="sentemail" name="sentemail">
                    
                </select>
            </span>
                
           </p>     
            <p>  
                <input type="hidden" id="EmailID" name="EmailID">
                
                <span class="span-9 first">
                <input type="submit" name="send_email_btn" id="send_email_btn" class="textSubmitButton"  value="Send" >
                
                <input type="reset" name="cancel_email_btn" id="cancel_email_btn" class="textSubmitButton"  value="Cancel" > 
                </span>
                <span class="span-9 first">
                    
                    <input type="submit" name="resend_email_btn" id="resend_email_btn" class="textSubmitButton"  value="Re-send Email" >
                    
                </span>
            </p>    
        </fieldset>    
        </form>
    </div>
    
</div>
