<?php

/**
 * AppiontmentType.class.php
 * 
 * Routines for interaction with the appointment_type table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 19/02/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class AppointmentType extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    

    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::AppointmentType();
    }
    
    /**
     * create
     *  
     * Create an appointment type
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new appointment
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Appointment Type successfully created */
                             'status' => 'SUCCESS',
                             'appointmentTypeId' => $this->conn->lastInsertId() /* Return the newly created appointment type's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'appointmentTypeId' => 0,                           /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * update
     *  
     * Update an appointment type
     * 
     * @param array $args   Associative array of field values for to update the
     *                      appointment type. The array must include the primary
     *                      key AppointmentTypeID
     * 
     * @return integer  Number of rows effected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'appointmentId' => $args['AppointmentID'],
                             'message' => ''
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete an entry from the appointments table. We can delete by  
     * AppointmentTypeID  To select the record to delete we must pass
     * a array with record with the key being either AppointmentID or SBAppointID
     * and tghe value being the appropriate ID to delete.
     * 
     * @param array $args (Field { AppointmentID, SBAppointID } => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($arg) {
        $index  = array_keys($arg);
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$arg[$index[0]] );
        
        if ($this->Execute($this->conn, $cmd, $arg)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'message' => 'Deleted'
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * getIdFromType
     *  
     * Returns the ID of an appointment type id for a given type
     * 
     * @param string $t     Appointment type
     * 
     * @return integer  AppointmentTypeID
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getIdFromType($t) {
        $sql = "
                SELECT
			`AppointmentTypeID`
		FROM
		        `appointment_type`
		WHERE
			`Type` = '$t'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if (count($result) == 0) {
            return(null);
        } else {
            return ($result[0]['AppointmentTypeID']);
        }
    
    }
    
    

}
?>
