<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Alternative Field Names under System Admin
 *
 * @author      Vic <v.rutkunas@pccsuk.com>
 * @version     1.0
 */


class AlternativeFieldNames extends CustomModel {
    
    private $conn;
    private $dbColumns = array(	't1.alternativeFieldID', 
				't3.primaryFieldName', 
				't1.alternativeFieldName',
				't1.status',
				't2.BrandName',
				't1.brandID');
    
    private $tables    = "  alternative_fields AS t1 
			    LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID 
			    LEFT JOIN primary_fields AS t3 ON t1.primaryFieldID=t3.primaryFieldID
			";
    private $table     = "alternative_fields";
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    
    public function fetch($args) {
        
	if ($this->controller->user->UserType=="Network") {
	    
	    $brands_model = $this->controller->loadModel("Brands");
	    $brands = $brands_model->getNetworkBrands($this->controller->user->NetworkID);
	    $brandsList  = implode(",", array_keys($brands));
	    $args['where'] = "t1.BrandID IN (" . $brandsList . ")";
	    
        } else if ($this->controller->user->UserType=="Client") {
	    
	    $brands_model = $this->controller->loadModel("Brands");
	    $brands = $brands_model->getClientBrands($this->controller->user->ClientID);
	    $brandsList  = implode(",", array_keys($brands));
	    $args['where'] = "t1.BrandID IN (" . $brandsList . ")";
	    
	}
        
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
	
	return $output;
        
    }
    
    
    
    public function processData($args) {
         
	if(!isset($args['alternativeFieldID']) || !$args['alternativeFieldID']) {
	    return $this->create($args);
        } else {
            return $this->update($args);
        }
	
    }
    
    
     
    public function create($args) {

	$q="INSERT INTO alternative_fields (primaryFieldID,
					    alternativeFieldName,
					    status,
					    brandID) 
	    VALUES			   (:primaryFieldID,
					    :alternativeFieldName,
					    :status,
					    :brandID)";
	
	$values	= array("primaryFieldID" => $args["primaryFieldID"],
			"alternativeFieldName" => $args["alternativeFieldName"],
			"status" => $args["status"],
			"brandID" => $args["brandID"]
		);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => $this->controller->page['data_inserted_msg']);
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
	
    }

    
    
    public function update($args) {
        
	$q="UPDATE  alternative_fields
	    SET	    alternativeFieldName=:alternativeFieldName,
		    status=:status
	    WHERE   alternativeFieldID=:alternativeFieldID";
	
	$values=array(	"alternativeFieldName" => $args["alternativeFieldName"],
			"status" => $args["status"],
			"alternativeFieldID" => $args["alternativeFieldID"]
		    );
	
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => $this->controller->page['data_updated_msg']);
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
	
    }
    
    
    
    public function fetchRow($args) {
        
        $sql = 'SELECT	    t1.alternativeFieldID,
			    t1.primaryFieldID,
			    t3.primaryFieldName, 
			    t1.alternativeFieldName, 
			    t1.status, 
			    t1.brandID,
			    t2.BrandName
		FROM	    ' . $this->table . ' AS t1
		LEFT JOIN   brand AS t2 ON t1.brandID=t2.BrandID
		LEFT JOIN   primary_fields AS t3 ON t1.primaryFieldID=t3.primaryFieldID 
		WHERE	    t1.alternativeFieldID=:alternativeFieldID';
	
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':alternativeFieldID' => $args['alternativeFieldID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }

    
    
    public function getAlternativeFields() {
	
	$q = "SELECT * FROM primary_fields";
	$result = $this->query($this->conn, $q, PDO::FETCH_CLASS  );
	return $result;
	
    }
    
    
    
    public function getBrandAlternativeFields($brandID) {
	
	$q = "SELECT * FROM alternative_fields WHERE brandID=:brandID";
	$values=array("brandID" => $brandID);
	$result = $this->query($this->conn, $q, $values );
	return $result;
	
    }
    

    
}
?>