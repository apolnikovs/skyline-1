/************************************************** 
 * 
 *  Updated for SkyLine project use
 *  
 *  - global functions moved top the top of file
 * 
 *  
 *   
 **************************************************/



function urlencode (str) {
            str = (str + '').toString();
            return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } 

(function( $ ){
    /* Get the rows which are currently selected */
    function fnGetSelected( oTableLocal ) {
        var aReturn = new Array();
        var aTrs = oTableLocal.fnGetNodes();

        for ( var i=0 ; i<aTrs.length ; i++ ) {
            if ( $(aTrs[i]).hasClass('row_selected') ) {
                aReturn.push( aTrs[i] );
            }
        }
        return aReturn;
    }

   function info() 
{
                var _info = new Array( 'SkyLine');
                return _info;
}

    function updateHeight() {
        if(location.href != top.location.href){ 
            // the content has been loaded into an IFRAME
            // so tell parent to resize IFRAME
            $.postMessage(
                    'if_height=' + $('body').outerHeight(true),
                    options['subdomain']+'/product/',
                    parent
            );
        }
    }

 




$.fn.PCCSDataTable = function( options ) {  

    // Create some defaults, extending them with any options that were provided
    var options = $.extend({
                        permission:'RWD',
                        htmlTableId: 'myBrowse',
                        addButtonId: 'example_AddButton',
                        updateButtonId: 'example_UpdateButton',
                        deleteButtonId: 'example_DeleteButton',
                        viewButtonId: 'example_ViewButton',
                        pickDataButtonId: 'example_PickDataButton',
                        formInsertButton: 'formInsertButton',
                        formUpdateButton: 'formUpdateButton',
                        formDeleteButton: 'formDeleteButton',
                        formCancelButton: 'formCancelButton',
                        formViewButton: 'formViewButton',
                        colorboxFormId: 'websitesForm',
                        /* fetchDataUrl: 'http://local.datatablespccs/data_fetch.php',
                        updateDataUrl: 'http://local.datatablespccs/data_update.php',
                        createDataUrl: 'http://local.datatablespccs/data_create.php',
                        deleteDataUrl: 'http://local.datatablespccs/data_delete.php',
                        createAppUrl: 'http://local.datatablespccs/app_create.php',
                        updateAppUrl: 'http://local.datatablespccs/app_update.php',
                        deleteAppUrl: 'http://local.datatablespccs/app_delete.php',*/
                        parentURL: '',
                        popUpFormWidth: 0, //Put 0 for default width
                        popUpFormHeight: 600, //Put 0 for default height
                        frmErrorRules: {},
                        frmErrorMessages: { },
                        frmErrorMsgClass: 'webformerror',
                        frmErrorElement: 'label',
                        // isExistsDataUrl: 'http://local.datatablespccs/app_isExists.php',
                        suggestTextId: 'suggestText',
                        sugestFieldId: 'WebsiteID',
                        frmErrorSugMsgClass: 'suggestwebformerror',
                        hiddenPK: 'pk',
                        passParamsType:'0',
                        superFilter: {},
                        subdomain: '',
                        pickDataIdList: new Array()

                    } , options);
                    
                    
        var oTable;    
        var passParamsTypeStr;
        
        //Primary key parmater passing type.
        if(options['passParamsType']=='1') {
               passParamsTypeStr = "?pk="; 
        } else {
               passParamsTypeStr = ""; 
        }                 

    return this.each(function() {    
            
        /* Add a click handler to the rows - this could be used as a callback */
        $('#'+options['htmlTableId']+' tbody').click(function(event) {
            if ($(event.target.parentNode).hasClass('row_selected')) {
                $(event.target.parentNode).removeClass('row_selected');
                $('#'+options['updateButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                $('#'+options['deleteButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                $('#'+options['viewButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                 $('#'+ options['pickDataButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
            } else {
                $(oTable.fnSettings().aoData).each(function () {
                    $(this.nTr).removeClass('row_selected');
                } );
                $(event.target.parentNode).addClass('row_selected');
                $('#'+options['updateButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                $('#'+options['deleteButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                $('#'+options['viewButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                  $('#'+ options['pickDataButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
            }            
        } );
        
        
        $(document).on('click', '#myBrowse_filter img',  function(){
            $(this).parent().find('input').val('').keyup();
        });

        /* Add a click handler to the add button of datatable */
        $(document).on('click', '#'+options['addButtonId'], 
            function() {

                //alert(options['createAppUrl']);

                // Ignore action if button disabled
                if (!$('#'+options['addButtonId']).attr('disabled')) {

                //It opens color box popup page.              
                $.colorbox( { href: options['createAppUrl'],
                                title: 'Add new record',
                                opacity: 0.75,
                                height:options['popUpFormHeight'],
                                width:options['popUpFormWidth'],
                                overlayClose: false,
                                escKey: false } );
                }
            }      
        );
            
        


                    /* Add a click handler to the update button of datatable */
                    $(document).on('click', '#'+options['updateButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['updateButtonId']).attr('disabled')) {
                                    var anSelected = fnGetSelected( oTable );


                                    //It opens color box popup page.  
                                    $.colorbox( { href:options['updateAppUrl']+passParamsTypeStr+oTable.fnGetData(anSelected[0],0),
                                                title: 'Update record',
                                                height:options['popUpFormHeight'],
                                                width:options['popUpFormWidth'],
                                                opacity: 0.75,     
                                                overlayClose: false,
                                                escKey: false,
                                                onComplete: function(){ $('#'+options['sugestFieldId']).attr('disabled', 'disabled'); }
                                        } );
                                }
                            }      
                        );



                        /* Add a click handler to the delete button of datatable */   
                        $(document).on('click', '#'+options['deleteButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['deleteButtonId']).attr('disabled')) {


                                var anSelected = fnGetSelected( oTable );

                                    //It opens color box popup page. 
                                    $.colorbox( { href:options['deleteAppUrl']+passParamsTypeStr+oTable.fnGetData(anSelected[0],0),
                                                title: 'Delete record',
                                                opacity: 0.75,     
                                                overlayClose: false,
                                                escKey: false } );


                                }
                            }      
                        );    


                          /* Add a click handler to the pick data button of datatable */   
                        $(document).on('click', '#'+options['pickDataButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['pickDataButtonId']).attr('disabled')) {

                                    var anSelected = fnGetSelected( oTable );

                                   
                                    for($i=0;$i<options['pickDataIdList'].length;$i++)
                                    {
                                        $(options['pickDataIdList'][$i]).val(oTable.fnGetData(anSelected[0],$i)).removeAttr('disabled');
                                    }
                                    $('#'+options['pickDataButtonId']).die('click');
                                    $.colorbox.close();

                                }
                            }      
                        );
/* Add a click handler to the view button of datatable */   
                        $(document).on('click', '#'+options['viewButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['viewButtonId']).attr('disabled')) {


                                var anSelected = fnGetSelected( oTable );

                                    //It opens color box popup page. 
                                    $.colorbox( { href:options['viewAppUrl']+passParamsTypeStr+oTable.fnGetData(anSelected[0],0),
                                                title: 'View record',
                                                opacity: 0.75,     
                                                overlayClose: false,
                                                escKey: false } );


                                }
                            }      
                        );                             




                        if(location.href != top.location.href){ 
                            // the content has been loaded into an IFRAME
                            // control full screen overlay in parent window when colorbox is opened/closed
                            $(document).bind('cbox_open', function() {

                                    $.postMessage(
                                        'showModalOverlay',
                                        options['parentURL'],
                                        parent
                                    );
                                } );
                            $(document).bind('cbox_closed', function() {
                                    $.postMessage(
                                        'hideModalOverlay',
                                        options['parentURL'],
                                        parent
                                    );
                                } );
                        }



                        /* Add a click handler to the delete button of colorbox popup form */  
                        $(document).on('click', '#'+options['formDeleteButton'], 
                            function() {

                            $.post(options['deleteDataUrl'],        

                                $("#"+options['colorboxFormId']).serialize(),      
                                function(data){
                                    // DATA NEXT SENT TO COLORBOX
                                    var p = eval("(" + data + ")");
                                $.fn.colorbox({
                                    html:   p['message'],
                                    onClosed:function(){ oTable.fnDraw(true); } 
                                    }); 
                                }); 

                            }
                            );


                        /* Add a click handler to the update button of colorbox popup form */  
                    $(document).on('click', '#'+options['formUpdateButton'], 
                            function() {
                                //Validating and posting form data.
                                $('#'+options['colorboxFormId']).validate({
                                            rules: options['frmErrorRules'],
                                            messages: options['frmErrorMessages'],               

                                            errorClass: options['frmErrorMsgClass'],
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: options['frmErrorElement'],

                                            submitHandler: function() {


                                                    $.post(options['updateDataUrl'],        

                                                    $("#"+options['colorboxFormId']).serialize(),      
                                                    function(data){
                                                        // DATA NEXT SENT TO COLORBOX
                                                        var p = eval("(" + data + ")");
                                                    $.fn.colorbox({
                                                        html:   p['message'],
                                                        onClosed:function(){ oTable.fnDraw(true); } 
                                                        }); 
                                                    });   

                                            }
                                });



                            }
                            );



                     //keyup sugestFieldId for check ID
                    $(document).on('keyup', '#'+options['sugestFieldId'], 
                            function() {

                                    if(!$('#'+options['hiddenPK']).val()) {

                                                $unique_txt = $('#'+options['sugestFieldId']).val();

                                                $.post(options['isExistsDataUrl'],{pk:$unique_txt},function(result){


                                                if(result=="null") {        

                                                $('#'+options['suggestTextId']).html(null);
                                                    $('#'+options['suggestTextId']).removeClass(options['frmErrorSugMsgClass']);
                                                } else {
                                                    $('#'+options['suggestTextId']).addClass(options['frmErrorSugMsgClass']);
                                                    $('#'+options['suggestTextId']).html('The '+options['sugestFieldId']+' you have entered is already exists in our database.');

                                                }
                                            });

                                }

                            });






                        /* Add a click handler to the insert button of colorbox popup form */  

                    $(document).on('click', '#'+options['formInsertButton'], function() {

                        //Validating and posting form data.

                        $('#'+options['colorboxFormId']).validate({
                            rules: options['frmErrorRules'],
                            messages: options['frmErrorMessages'],               

                            errorClass: options['frmErrorMsgClass'],
                            onkeyup: false,
                            onblur: false,
                            errorElement: options['frmErrorElement'],

                            submitHandler: function() {

                                    if(!$('#'+options['suggestTextId']).html()) {

                                        $.post(options['createDataUrl'],        

                                        $("#"+options['colorboxFormId']).serialize(),      
                                        function(data){
                                            // DATA NEXT SENT TO COLORBOX
                                            var p = eval("(" + data + ")");
                                        $.fn.colorbox({
                                            html:   p['message'],
                                            onClosed:function(){ oTable.fnDraw(true); } 
                                            }); 
                                        });    
                                    }// end if

                                }// end submitHandler
                       }); // validate
                    });



                        /* Add a click handler to the cancel button of colorbox popup form */  
                        $(document).on('click', '#'+options['formCancelButton'], 
                            function() {
                                $.colorbox.close();
                            }      
                        );


                           

                        




                        jQuery.fn.dataTableExt.oApi.fnFilterOnReturn = function (oSettings) {
                            /*
                            * Usage:       $('#example').dataTable().fnFilterOnReturn();
                            * Author:      Jon Ranes (www.mvccms.com)
                            * License:     GPL v2 or BSD 3 point style
                            * Contact:     jranes /AT\ mvccms.com
                            */
                            var _that = this;

                            this.each(function (i) {
                                $.fn.dataTableExt.iApiIndex = i;
                                var $this = this;
                                var anControl = $('input', _that.fnSettings().aanFeatures.f);
                                anControl.unbind('keyup').bind('keypress', function (e) {
                                    if (e.which == 13) {
                                        $.fn.dataTableExt.iApiIndex = i;
                                        _that.fnFilter(anControl.val());
                                    }
                                });
                                return this;
                            });
                            return this;
                        }            




                        oTable = $('#'+options['htmlTableId']).dataTable( {
                           
                           "aoColumns": options['aoColumns'],
                           'sDom': 'ft<"dataTables_command">rpli',
                            'sPaginationType': 'full_numbers',
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page"
                            },
                            'fnDrawCallback': function() {
                                $('.dataTables_command').html(function(){
                                    var create_but = '<button id="'+options['addButtonId']+'" type="button" class="gplus-blue"><span class="label">Add</span></button>';
                                    var update_but = '<button id="'+options['updateButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">Update</span></button>';
                                    var delete_but = '<button id="'+ options['deleteButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">Delete</span></button>';
                                    var view_but = '<button id="'+ options['viewButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">View</span></button>';
                                    var pick_but = '<button id="'+ options['pickDataButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">Pick</span></button>';
                                    
                                    var html_buttons = '';
                                    switch(options['permission']){
                                        case'RWD':
                                            html_buttons = create_but + update_but + delete_but + view_but;
                                            break;
                                        case'RW':
                                            html_buttons = create_but + update_but + view_but + pick_but;
                                            break;
                                        case'R':
                                            html_buttons = view_but + pick_but;
                                            break;                                            
                                        default:
                                            html_buttons = '';
                                    }
                                    
                                    return html_buttons;
                                });  
                            },
                            'bServerSide': true,
                            'bProcessing': true,
                            'bDeferRender': true,
                            'sAjaxSource': options['fetchDataUrl'],
                            'fnServerData': function ( sSource, aoData, fnCallback ) {
                                                
                                                //unserialize aoData
                                                if(options.superFilter != '')
                                                    aoData = $.param(aoData, true) + '&' + $.param(options.superFilter, true);

                                                $.ajax( { 'dataType': 'json', 
                                                        'type': 'POST', 
                                                        'cache': false,
                                                        'url': sSource, 
                                                        'data': aoData, 
                                                        'success': fnCallback,
                                                        'complete': function() { updateHeight(); } 
                                                } );
                                            }

                        } );
                        
                        $('#myBrowse_filter input').wrap('<span></span>').after('<img src="'+options['subdomain']+'/images/close.png" alt="" />');    


    });

};//$.fn.PCCSDataTable
})( jQuery );
